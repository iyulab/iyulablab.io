$(window).scroll(function(){
    var wScroll = $(this).scrollTop();

    $('.logo').css({
        'transform' : 'translate(0px, '+ wScroll /15 +'%)'
    });

    $('.back-bird').css({
        'transform' : 'translate(0px, '+ wScroll /4 +'%)'
    });

    $('.fore-bird').css({
        'transform' : 'translate(0px, -'+ wScroll /30 +'%)'
    });


    if(wScroll > $('.technology').offset().top - ($(window).height() / 2.0)) {
        $('.technology figure').each(function(i){
            setTimeout(function() {
            $('.technology figure').eq(i).addClass('is-showing'); 
            }, 150 * (i+1));
                    
        });
    }

    if(wScroll > $('.footer-stuff').offset().top - ($(window).height() / 2.0)) {
        $("#row footer-stuff").each(function(){
            $("h1").hide();
            console.log(h1)
        });
        $("#row footer-stuff").each(function(){
            $("h1").show();
        });  
    }


    if(wScroll > $('.product').offset().top - $(window).height()){
        $('.product').css({'background-position':'center' + (wScroll - $('.product').offset().top +'px')});

        var opacity = (wScroll - $('.product').offset().top + 700) / (wScroll / 5)

        $('.window-tint').css({'opacity': opacity})
    }

  if(wScroll > $('.team').offset().top - $(window).height()){

        var offset = Math.min(0, wScroll - $('.team').offset().top + $(window).height() - 350);

        $('.post-1').css({'transform': 'translate('+ offset + 'px,' + Math.abs(offset * 0.2) + 'px)'});

        $('.post-3').css({'transform': 'translate('+ Math.abs(offset) + 'px, ' + Math.abs(offset * 0.2) + 'px)'});

    }
});

